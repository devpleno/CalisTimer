import React from 'react';
import { View, Text } from 'react-native';
import { createStackNavigator, createAppContainer } from 'react-navigation'

import HomeScreen from './src/screens/HomeScreen';
import EmomScreen from './src/screens/EmomScreen';
import AmrapScreen from './src/screens/AmrapScreen';
import IsometriaScreen from './src/screens/IsometriaScreen';
import SobreScreen from './src/screens/SobreScreen';

const AppNavigator = createStackNavigator({
  Isometria: IsometriaScreen,
  Amrap: AmrapScreen,
  Emom: EmomScreen,
  Home: HomeScreen,
  Sobre: SobreScreen
}, { initialRouteName: 'Home' })

export default createAppContainer(AppNavigator)