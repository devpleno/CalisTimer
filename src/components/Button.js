import React from 'react'
import { Text, StyleSheet, TouchableHighlight } from 'react-native'

const Button = (props) => {
    return (
        <TouchableHighlight onPress={props.onPress} style={styles.menu}>
            <Text style={styles.txtMenu}>{props.children}</Text>
        </TouchableHighlight>
    )
}

const styles = StyleSheet.create({
    menu: {
        padding: 30,
    },
    txtMenu: {
        fontFamily: 'Ubuntu',
        color: 'white',
        fontSize: 24,
    }
});

export default Button