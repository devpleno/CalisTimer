import React, { Component } from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'

class Select extends Component {

    state = {
        current: ''
    }

    componentDidMount() {
        this.setState({
            current: this.props.current
        })
    }

    handlePress = (opt) => () => {
        const { current } = this.state

        if (Array.isArray(current)) {
            let newCurrent = current

            let i = current.indexOf(opt)

            if (i >= 0) {
                newCurrent = [...current]
                newCurrent.splice(i, 1)
            } else {
                newCurrent = [...current, opt]
            }

            this.setState({ current: newCurrent })

            if (this.props.onSelect) {
                this.props.onSelect(newCurrent)
            }
        } else {
            this.setState({ current: opt })

            if (this.props.onSelect) {
                this.props.onSelect(opt)
            }
        }
    }

    checkItem = (item) => {
        const { current } = this.state

        if (Array.isArray(current)) {
            return current.indexOf(item) >= 0
        }

        return current === item
    }

    render() {
        const { options, label } = this.props

        return (
            <View>
                <Text style={styles.label}>{label}</Text>

                <View style={styles.linha}>
                    {options.map(opt => {

                        let id = ''
                        let label = ''

                        if (typeof opt === 'string') {
                            id = opt
                            label = opt
                        }

                        if (typeof opt === 'object') {
                            id = opt['id']
                            label = opt['label']
                        }

                        return (
                            <TouchableOpacity style={[this.checkItem(id) ? styles.optSelected : null]} key={id} onPress={this.handlePress(id)}>
                                <Text style={styles.optLabel}>{label}</Text>
                            </TouchableOpacity>
                        )
                    })}
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    linha: {
        flexDirection: 'row',
        justifyContent: 'space-around',
    },
    optSelected: {
        backgroundColor: '#FFFFFF60',
    },
    label: {
        textAlign: 'center',
        color: 'white',
        fontSize: 20
    },
    opt: {
    },
    optLabel: {
        color: 'white',
        fontSize: 25,
        padding: 20,
    }
});

export default Select