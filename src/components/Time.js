import React from 'react'
import { Text, View, StyleSheet, TouchableHighlight } from 'react-native'

const Time = (props) => {

    const minutes = parseInt(props.time / 60)
    const seconds = parseInt(props.time % 60)

    const format = (num) => {
        if (num < 10) {
            return '0' + num
        }

        return num
    }

    return (
        <View>
            <Text style={[styles.valor, (props.style ? { ...props.style } : '')]}>
                {format(minutes)}:{format(seconds)} {props.texto ? props.texto : ''}
            </Text>
        </View>
    )
}

const styles = StyleSheet.create({
    valor: {
        fontSize: 96,
        color: 'white',
        alignSelf: 'center',
        fontFamily: 'Ubuntu-Bold',
    },
});

export default Time