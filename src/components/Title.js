import React from 'react'
import { Text, View, StyleSheet } from 'react-native'

const Title = (props) => {
    return (
        <View style={[styles.container, props.style]}>
            <Text style={styles.titulo}>{props.titulo}</Text>
            <Text style={styles.subtitulo}>{props.subtitulo}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
    },
    titulo: {
        fontSize: 48,
        color: 'white',
        fontFamily: 'Ubuntu-Bold',
    },
    subtitulo: {
        fontFamily: 'Ubuntu',
        color: 'white',
        fontSize: 24,
        marginBottom: 100,

    },
});

export default Title