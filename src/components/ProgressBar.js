import React, { Component } from 'react'
import { View, Text, StyleSheet, Animated } from 'react-native'

class ProgressBar extends Component {

    constructor(props) {
        super(props)

        this.width = new Animated.Value(0)
    }

    componentDidUpdate(prevProps) {
        if (prevProps.percentage !== this.props.percentage) {
            Animated.timing(this.width, {
                toValue: this.props.percentage,
                duration: 500
            }).start()
        }
    }

    render() {
        const { percentage } = this.props

        const w = this.width.interpolate({
            inputRange: [0, 100],
            outputRange: ['0%', '100%']
        })

        return (
            <View>
                <Animated.View style={[styles.barra, { width: w }]}>
                    {percentage > 10 &&
                        <Text style={{ textAlign: 'right', marginRight: 5 }}>{percentage} %</Text>
                    }
                </Animated.View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    barra: {
        backgroundColor: 'white',
        height: 20
    },
});

export default ProgressBar