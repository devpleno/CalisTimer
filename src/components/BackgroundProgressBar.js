import React, { Component } from 'react'
import { View, Text, StyleSheet, Animated } from 'react-native'

class BackgroundProgressBar extends Component {

    constructor(props) {
        super(props)

        this.height = new Animated.Value(0)
    }

    componentDidUpdate(prevProps) {
        if (prevProps.percentage !== this.props.percentage) {
            Animated.timing(this.height, {
                toValue: this.props.percentage > 100 ? 100 : this.props.percentage,
                duration: 500
            }).start()
        }
    }

    render() {

        const { children, percentage } = this.props

        const h = this.height.interpolate({
            inputRange: [0, 100],
            outputRange: ['0%', '100%']
        })

        return (
            <View style={{ flex: 1 }}>
                <View style={{ flex: 1 }}>
                    <View style={styles.barra1} />

                    {percentage > 0 &&
                        <Text style={styles.percentage}>{percentage} %</Text>
                    }
                    <Animated.View style={[styles.barra2, { height: h }]} />
                </View>
                <View style={styles.container}>
                    {children}
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    barra1: {
        flex: 1,
        backgroundColor: '#D63049',
    },
    barra2: {
        backgroundColor: '#2A0E12',
    },
    container: {
        position: 'absolute',
        left: 0,
        top: 0,
        bottom: 0,
        right: 0,
    },
    percentage: {
        backgroundColor: '#2A0E12',
        color: 'white',
        textAlign: 'right',
    }
});

export default BackgroundProgressBar