export default assets = {
    'gear': require('../../assets/images/gear.png'),
    'play': require('../../assets/images/play.png'),
    'refresh': require('../../assets/images/refresh.png'),
    'devreactjs': require('../../assets/images/devreactjs.png'),
    'stop': require('../../assets/images/stop.png')
}