import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, StyleSheet, Linking } from 'react-native'

import Title from '../components/Title'

class SobreScreen extends Component {

    handleVoltar = () => {
        this.props.navigation.goBack()
    }

    abrirNavegador = (url) => {
        Linking.openURL(url)
    }

    render() {
        return (
            <View style={styles.container}>
                <Title titulo='Sobre' style={{ justifyContent: 'center', marginTop: 100 }} />

                <Text style={styles.conteudo}>Este aplicativos foi desenvolvido durante o curso de DevReactJS no portal do DevPleno.</Text>

                <TouchableOpacity onPress={() => this.abrirNavegador('https://www.devpleno.com.br/')} style={{ flex: 1, justifyContent: 'center' }}>
                    <Image source={assets.devreactjs} style={{ alignSelf: 'center', marginTop: 100 }} />
                </TouchableOpacity>

                <TouchableOpacity onPress={() => this.handleVoltar()} style={{ flex: 1, justifyContent: 'center' }}>
                    <Text style={styles.voltar}>Voltar</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

SobreScreen.navigationOptions = {
    header: null
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#D63049',
    },
    conteudo: {
        color: 'white',
        fontSize: 20,
        textAlign: 'justify',
        width: '50%',
    },
    voltar: {
        color: 'white',
        fontSize: 20,
        padding: 20,
        textAlign: 'center',
    }
})

export default SobreScreen