import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import Button from '../components/Button'

const HomeScreen = (props) => {
    return (
        <View style={styles.container}>
            <Text style={styles.welcome}>CalisTimer</Text>

            <Button onPress={() => props.navigation.navigate('Emom')}>EMOM</Button>
            <Button onPress={() => props.navigation.navigate('Amrap')}>AMRAP</Button>
            <Button onPress={() => props.navigation.navigate('Isometria')}>Isometria</Button>
            <Button onPress={() => props.navigation.navigate('Sobre')}>Sobre</Button>
        </View>
    )
}

HomeScreen.navigationOptions = {
    header: null
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#D63049',
    },
    welcome: {
        fontSize: 48,
        textAlign: 'center',
        marginBottom: 200,
        color: 'white',
        fontFamily: 'Ubuntu-Bold',
    }
});

export default HomeScreen