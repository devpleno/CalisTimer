import React, { Component } from 'react'
import { Platform, Keyboard, ScrollView, View, Text, TextInput, Image, StyleSheet, TouchableOpacity, KeyboardAvoidingView } from 'react-native'

import KeepAwake from 'react-native-keep-awake'

import Sound from 'react-native-sound'
const alert = require('../../assets/sounds/alarm.wav')

import Select from '../components/Select';
import Title from '../components/Title';

import assets from './assets'
import Time from '../components/Time';
import BackgroundProgressBar from '../components/BackgroundProgressBar';

class IsometriaScreen extends Component {

    state = {
        keyboardIsVisible: false,
        objetivos: 0,
        countdown: 0,
        countdownValue: 5,
        count: 0,
        countTimer: 0,
        time: '20',
        paused: false,
        isRunning: false,
        seg: 1000,

        selectObjetivo: [
            { id: 0, label: 'livre' },
            { id: 1, label: 'bater tempo' },
        ],

        selectCR: [
            { id: 0, label: 'Não' },
            { id: 1, label: 'Sim' },
        ]
    }

    componentDidMount() {
        Sound.setCategory('Playback', true)
        this.alert = new Sound(alert)

        this.knShow = Keyboard.addListener('keyboardDidShow', () => {
            this.setState({
                keyboardIsVisible: true
            })
        })

        this.knHide = Keyboard.addListener('keyboardDidHide', () => {
            this.setState({
                keyboardIsVisible: false
            })
        })
    }

    componentWillUnmount() {
        this.knShow.remove();
        this.knHide.remove();
    }

    playAlert = () => {
        const { count, time } = this.state

        if (count >= (parseInt(time) - 5) && count <= (parseInt(time))) {
            this.alert.play();
        }
    }

    handlePlay = (seg) => {
        this.setState({ paused: false, isRunning: true, seg: seg })

        const count = () => {
            if (this.state.paused) {
                return;
            }

            this.setState({ count: this.state.count + 1 }, () => {
                this.playAlert();
            })
        }

        if (this.state.countdown === 1) {
            if (this.state.paused) {
                return;
            }

            this.alert.play()

            this.cTimer = setInterval(() => {
                this.alert.play()

                this.setState({ countdownValue: this.state.countdownValue - 1 }, () => {
                    if (this.state.countdownValue === 0) {
                        clearInterval(this.cTimer)
                        this.countTimer = setInterval(count, seg)
                    }
                })
            }, seg)
        } else {
            this.countTimer = setInterval(count, seg)
        }

    }

    handleRefresh = () => {
        this.setState({ countdownValue: 5, count: 0 })
        clearInterval(this.countTimer)
        clearInterval(this.cTimer)

        this.handlePlay(this.state.seg)
    }

    handleVoltar = () => {
        this.setState({ paused: false, isRunning: false, countdownValue: 5, count: 0 })
        clearInterval(this.countTimer)
        clearInterval(this.cTimer)
    }

    voltarInicio = () => {
        this.props.navigation.goBack()
    }

    handlePause = () => {
        this.setState({ paused: !this.state.paused })
    }

    render() {

        if (this.state.isRunning) {
            const percMinute = parseInt((this.state.count / parseInt(this.state.time)) * 100)
            const restante = parseInt(this.state.time) - this.state.count

            return (
                <BackgroundProgressBar percentage={percMinute}>
                    <KeepAwake />

                    <View style={[styles.container, { justifyContent: 'center', }]}>

                        <View style={{ flex: 1, justifyContent: 'center' }}>
                            <Title titulo='Isometria' style={{ justifyContent: 'center' }} />
                        </View>

                        {this.state.countdown === 1 && this.state.countdownValue > 0 &&
                            <View style={{ flex: 1, justifyContent: 'center' }}>
                                <Text style={styles.regressivo}>{this.state.countdownValue}</Text>
                            </View>
                        }

                        {(this.state.countdown === 0 || this.state.countdownValue === 0) &&
                            <View style={{ flex: 1 }}>
                                <Time time={this.state.count} />

                                {restante > 0 && this.state.objetivos === 1 &&
                                    <Time time={restante} style={{ fontSize: 20 }} texto='restantes' />
                                }

                                <View style={{ flexDirection: 'row' }}>
                                    <TouchableOpacity onPress={() => this.handleVoltar()} style={{ flex: 1, justifyContent: 'center', }}>
                                        <Text style={styles.voltar}>Voltar</Text>
                                    </TouchableOpacity>

                                    <TouchableOpacity onPress={() => this.handlePause()} style={{ flex: 1, justifyContent: 'center' }}>
                                        {this.state.paused ?
                                            <Image source={assets.play} style={{ width: 100, height: 100, margin: 20, alignSelf: 'center' }} />
                                            :
                                            <Image source={assets.stop} style={{ width: 100, height: 100, margin: 20, alignSelf: 'center' }} />
                                        }
                                    </TouchableOpacity>

                                    <TouchableOpacity onPress={() => this.handleRefresh()} style={{ flex: 1, justifyContent: 'center' }}>
                                        <Image source={assets.refresh} style={{ width: 50, height: 50, margin: 20, alignSelf: 'center' }} />
                                    </TouchableOpacity>
                                </View>
                            </View>
                        }
                    </View>
                </BackgroundProgressBar>
            )
        }

        const { selectObjetivo, selectCR } = this.state

        const behavior = Platform.OS === 'ios' ? 'padding' : 'height'
        const paddingTop = Platform.OS === 'ios' ? (this.state.keyboardIsVisible ? 20 : 200) : 50

        return (
            <KeyboardAvoidingView style={{ flex: 1 }} behavior={behavior}>
                <ScrollView style={[styles.container, { backgroundColor: '#D63049' }]}>

                    <Title titulo='Isometria' style={{ paddingTop: paddingTop }} />

                    <Image source={assets.gear} style={{ width: 50, height: 50, marginBottom: 20, alignSelf: 'center' }} />

                    <Select label='Objetivo' current={this.state.objetivos} options={selectObjetivo} onSelect={opt => this.setState({ objetivos: opt })} />

                    <Select label='Contagem regressiva' current={this.state.countdown} options={selectCR} onSelect={opt => this.setState({ countdown: opt })} />

                    {this.state.objetivos === 1 &&
                        <React.Fragment>
                            <Text style={styles.label}>Quantos segundos ?</Text>
                            <TextInput keyboardType='numeric' style={styles.input} value={this.state.time} onChangeText={txt => this.setState({ time: txt })} />
                        </React.Fragment>
                    }

                    <View style={{ flexDirection: 'row' }}>
                        <TouchableOpacity onPress={() => this.voltarInicio()} style={{ flex: 1, justifyContent: 'center', }}>
                            <Text style={styles.voltar}>Voltar</Text>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => this.handlePlay(1000)} style={{ flex: 1, }}>
                            <Image source={assets.play} style={{ width: 100, height: 100, margin: 20, alignSelf: 'center' }} />
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => this.handlePlay(100)} style={{ flex: 1, justifyContent: 'center', }}>
                            <Text style={styles.testar}>Testar</Text>
                        </TouchableOpacity>
                    </View>

                </ScrollView>
            </KeyboardAvoidingView>
        )
    }
}

IsometriaScreen.navigationOptions = {
    header: null
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    regressivo: {
        color: 'orange',
        textAlign: 'center',
        fontSize: 100
    },
    input: {
        textAlign: 'center',
        color: 'black',
        fontSize: 48
    },
    label: {
        textAlign: 'center',
        color: 'white',
        fontSize: 20,
    },
    testar: {
        color: 'white',
        fontSize: 20,
        padding: 20,
        textAlign: 'center',
    },
    voltar: {
        color: 'white',
        fontSize: 20,
        padding: 20,
        textAlign: 'center',
    }
});

export default IsometriaScreen