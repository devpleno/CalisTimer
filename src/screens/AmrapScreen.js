import React, { Component } from 'react'
import { Platform, Keyboard, ScrollView, View, Text, TextInput, Image, StyleSheet, TouchableOpacity, KeyboardAvoidingView } from 'react-native'

import KeepAwake from 'react-native-keep-awake'

import Sound from 'react-native-sound'
const alert = require('../../assets/sounds/alarm.wav')

import Select from '../components/Select';
import Title from '../components/Title';

import assets from './assets'
import Time from '../components/Time';
import ProgressBar from '../components/ProgressBar';
import BackgroundProgressBar from '../components/BackgroundProgressBar';

class AmrapScreen extends Component {

    state = {
        keyboardIsVisible: false,
        alerts: [0, 30],
        countdown: 0,
        countdownValue: 5,
        count: 0,
        countTimer: 0,
        time: '2',
        isRunning: false,
        paused: false,
        repetitions: 0,
        seg: 1000,

        selectAlertas: [
            { id: 0, label: 'OFF' },
            { id: 15, label: '15s' },
            { id: 30, label: '30s' },
            { id: 45, label: '45s' }
        ],

        selectCR: [
            { id: 0, label: 'Não' },
            { id: 1, label: 'Sim' },
        ]
    }

    componentDidMount() {
        Sound.setCategory('Playback', true)
        this.alert = new Sound(alert)

        this.knShow = Keyboard.addListener('keyboardDidShow', () => {
            this.setState({
                keyboardIsVisible: true
            })
        })

        this.knHide = Keyboard.addListener('keyboardDidHide', () => {
            this.setState({
                keyboardIsVisible: false
            })
        })
    }

    componentWillUnmount() {
        this.knShow.remove();
        this.knHide.remove();
    }

    playAlert = () => {
        const resto = this.state.count % 60

        if (this.state.alerts.indexOf(resto) > -1) {
            this.alert.play();
        }

        if (this.state.countdown === 1) {
            if (resto >= 55 && resto < 60) {
                this.alert.play();
            }
        }
    }

    handlePlay = (seg) => {
        this.setState({ isRunning: true, paused: false, seg: seg })

        const count = () => {
            if (this.state.paused) {
                return;
            }

            this.setState({ count: this.state.count + 1 }, () => {
                this.playAlert();

                if (this.state.count === parseInt(this.state.time) * 60) {
                    clearInterval(this.countTimer)
                }
            })
        }

        if (this.state.countdown === 1) {
            if (this.state.paused) {
                return;
            }

            this.alert.play()

            this.cTimer = setInterval(() => {
                this.alert.play()

                this.setState({ countdownValue: this.state.countdownValue - 1 }, () => {
                    if (this.state.countdownValue === 0) {
                        clearInterval(this.cTimer)
                        this.countTimer = setInterval(count, seg)
                    }
                })
            }, seg)
        } else {
            this.countTimer = setInterval(count, seg)
        }

    }

    menos = () => {
        if (this.state.repetitions > 0) {
            this.setState({
                repetitions: this.state.repetitions - 1
            })
        }
    }

    mais = () => {
        this.setState({
            repetitions: this.state.repetitions + 1
        })
    }

    handleRefresh = () => {
        this.setState({ countdownValue: 5, count: 0, repetitions: 0 })
        clearInterval(this.countTimer)
        clearInterval(this.cTimer)

        this.handlePlay(this.state.seg)
    }

    handleVoltar = () => {
        this.setState({ paused: false, isRunning: false, countdownValue: 5, count: 0 })
        clearInterval(this.countTimer)
        clearInterval(this.cTimer)
    }

    voltarInicio = () => {
        this.props.navigation.goBack()
    }

    handlePause = () => {
        this.setState({ paused: !this.state.paused })
    }

    render() {

        if (this.state.isRunning) {

            const percMinute = parseInt(((this.state.count % 60) / 60) * 100)
            const percTime = parseInt(((this.state.count / 60) / parseInt(this.state.time)) * 100)

            const media = (this.state.repetitions > 0) ? this.state.count / this.state.repetitions : 0
            const estimated = (media > 0) ? Math.floor((parseInt(this.state.time) * 60) / media) : 0

            return (
                <BackgroundProgressBar percentage={percMinute}>
                    <KeepAwake />

                    <View style={[styles.container, { justifyContent: 'center', }]}>

                        <View style={{ flex: 1, justifyContent: 'center' }}>
                            <Title titulo='Amrap' subtitulo='As many repetitions As possible' style={{ justifyContent: 'center' }} />
                        </View>

                        {this.state.countdown === 1 && this.state.countdownValue > 0 &&
                            <View style={{ flex: 1, justifyContent: 'center' }}>
                                <Text style={styles.regressivo}>{this.state.countdownValue}</Text>
                            </View>
                        }

                        {(this.state.countdown === 0 || this.state.countdownValue === 0) &&
                            <View style={{ flex: 1 }}>

                                {this.state.repetitions > 0 &&
                                    <View style={{ flexDirection: 'row' }}>
                                        <View style={{ flex: 1 }}>
                                            <Time time={media} style={styles.menor2} />
                                            <Text style={styles.subtitle}>por repetição</Text>
                                        </View>
                                        <View style={{ flex: 1 }}>
                                            <Text style={styles.menor2}>{estimated}</Text>
                                            <Text style={styles.subtitle}>repetições</Text>
                                        </View>
                                    </View>
                                }

                                <Time time={this.state.count} style={{ fontSize: 96 }} />
                                <ProgressBar percentage={percTime} />
                                <Time time={parseInt(this.state.time) * 60 - this.state.count} style={{ fontSize: 20 }} texto='restantes' />

                                <View style={{ flexDirection: 'row' }}>
                                    <TouchableOpacity onPress={() => this.menos()} style={{ flex: 1, justifyContent: 'center', }}>
                                        <Text style={styles.sinal}>-</Text>
                                    </TouchableOpacity>

                                    <Text style={styles.sinal}>{this.state.repetitions}</Text>

                                    <TouchableOpacity onPress={() => this.mais()} style={{ flex: 1, justifyContent: 'center', }}>
                                        <Text style={styles.sinal}>+</Text>
                                    </TouchableOpacity>
                                </View>

                                <View style={{ flexDirection: 'row' }}>
                                    <TouchableOpacity onPress={() => this.handleVoltar()} style={{ flex: 1, justifyContent: 'center', }}>
                                        <Text style={styles.voltar}>Voltar</Text>
                                    </TouchableOpacity>

                                    <TouchableOpacity onPress={() => this.handlePause()} style={{ flex: 1, justifyContent: 'center' }}>
                                        {this.state.paused ?
                                            <Image source={assets.play} style={{ width: 100, height: 100, margin: 20, alignSelf: 'center' }} />
                                            :
                                            <Image source={assets.stop} style={{ width: 100, height: 100, margin: 20, alignSelf: 'center' }} />
                                        }
                                    </TouchableOpacity>

                                    <TouchableOpacity onPress={() => this.handleRefresh()} style={{ flex: 1, justifyContent: 'center' }}>
                                        <Image source={assets.refresh} style={{ width: 50, height: 50, margin: 20, alignSelf: 'center' }} />
                                    </TouchableOpacity>
                                </View>
                            </View>
                        }
                    </View>
                </BackgroundProgressBar>
            )
        }

        const { selectAlertas, selectCR } = this.state

        const behavior = Platform.OS === 'ios' ? 'padding' : 'height'
        const paddingTop = Platform.OS === 'ios' ? (this.state.keyboardIsVisible ? 20 : 200) : 50

        return (
            <KeyboardAvoidingView style={{ flex: 1 }} behavior={behavior}>
                <ScrollView style={[styles.container, { backgroundColor: '#D63049' }]}>

                    <Title titulo='Amrap' subtitulo='As many repetitions As possible' style={{ paddingTop: paddingTop }} />

                    <Image source={assets.gear} style={{ width: 50, height: 50, marginBottom: 20, alignSelf: 'center' }} />

                    <Select label='Alertas' current={this.state.alerts} options={selectAlertas} onSelect={opt => this.setState({ alerts: opt })} />

                    <Select label='Contagem regressiva' current={this.state.countdown} options={selectCR} onSelect={opt => this.setState({ countdown: opt })} />

                    <Text style={styles.label}>Quantos minutos ?</Text>

                    <TextInput keyboardType='numeric' style={styles.input} value={this.state.time} onChangeText={txt => this.setState({ time: txt })} />

                    <View style={{ flexDirection: 'row' }}>
                        <TouchableOpacity onPress={() => this.voltarInicio()} style={{ flex: 1, justifyContent: 'center', }}>
                            <Text style={styles.voltar}>Voltar</Text>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => this.handlePlay(1000)} style={{ flex: 1, }}>
                            <Image source={assets.play} style={{ width: 100, height: 100, margin: 20, alignSelf: 'center' }} />
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => this.handlePlay(100)} style={{ flex: 1, justifyContent: 'center', }}>
                            <Text style={styles.testar}>Testar</Text>
                        </TouchableOpacity>
                    </View>

                </ScrollView>
            </KeyboardAvoidingView>
        )
    }
}

AmrapScreen.navigationOptions = {
    header: null
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    regressivo: {
        color: 'orange',
        textAlign: 'center',
        fontSize: 100
    },
    input: {
        textAlign: 'center',
        color: 'black',
        fontSize: 48
    },
    label: {
        textAlign: 'center',
        color: 'white',
        fontSize: 20,
    },
    testar: {
        color: 'white',
        fontSize: 20,
        padding: 20,
        textAlign: 'center',
    },
    voltar: {
        color: 'white',
        fontSize: 20,
        padding: 20,
        textAlign: 'center',
    },
    sinal: {
        color: 'white',
        fontSize: 50,
        fontWeight: 'bold',
        padding: 20,
        textAlign: 'center',
    },
    menor2: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 30,
        textAlign: 'center',
    },
    subtitle: {
        color: 'white',
        fontSize: 30,
        textAlign: 'center',
    }
});

export default AmrapScreen